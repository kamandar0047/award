package az.neft.mste.neft.controller;

import az.neft.mste.neft.services.StuService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static java.nio.file.Paths.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(StuController.class)
class StuControllerTest {
private static final String MAIN_URL ="/v1/stu";
    private static final String DUMMY_ID ="1";
    @MockBean
    private StuService stuService;


@Autowired
private MockMvc mockMvc;
/*
    @Test
    void whenGetThenOk() twrows Exception {
mockMvc.perform(get(MAIN_URL+"/"+DUMMY_ID)
      //  .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status()).isOk()
        .andDo(print());
    }

 */
    @Test
    void create() {
    }

    @Test
    void update() {
    }

    @Test
    void delete() {
    }
}