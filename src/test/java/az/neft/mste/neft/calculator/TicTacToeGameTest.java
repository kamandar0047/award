package az.neft.mste.neft.calculator;

import az.neft.mste.neft.domain.TicTacToeDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TicTacToeGameTest {


    @Mock
    private TicTacToeDao dao;
    @InjectMocks
    private TicTacToeGame game;

@Captor
    private ArgumentCaptor<Date> dateArgumentCaptor;



//    @BeforeEach
  //  void setUp(){

    //    game=new TicTacToeGame(dao);
    //}

    private char lastPlayer = 'X';

    public char nextPlayer() {

        return lastPlayer;
    }


    @Test
    void whenXOutsideTheBoardThenException() {
        assertThatThrownBy(() -> game.play(4, 1, lastPlayer))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("XCord is outside");
    }

    @Test
    void whenYOutsideTheBoardThenException() {
        assertThatThrownBy(() -> game.play(1, 4, lastPlayer))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("YCord is outside");
    }

    @Test
    void whenBoardISOccupiedThenException() {

        game.play(1, 1, lastPlayer);
        assertThatThrownBy(() -> game.play(1, 1, lastPlayer))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Board is occupied");
    }

    @Test
    public void givenFirstTurnWhenNextPlayerThenX() {
      game.play(1, 1);
        //  assertThat(game.nextPlayer()).isEqualTo('0');

//   assertThat(game.nextPlayer()).isEqualTo('0');
    verify(dao,atLeast(2)).printResult(dateArgumentCaptor.capture());
        System.out.println(dateArgumentCaptor);

  //  assertThat(dateArgumentCaptor.getValue()).isNotNull();

    }

    @Test
    public void givenLastTurnWhenNextPlayerThen0() {
        game.play(1, 1, lastPlayer);
        //   assertThat(game.nextPlayer()).isEqualTo('0');

        assertThatThrownBy(() -> game.play(1,1))
                .isInstanceOf(RuntimeException.class);
  //      assertThat(game.nextPlayer()).isEqualTo('0')
    }

    @Test
    public void whenPlayThenNoWinner() {

        // assertThat(() -> game.play(1,1)).isEqualTo("No winner");
    }

}