package az.neft.mste.neft.calculator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MyCalculatorTest {

    @Mock
    private MyCalculatorDao myCalculatorDao;
    @InjectMocks
    private MyCalculator myCalculator;

    @Test
    public void givenAAndBWhenAddThenC() {

        int a = 5;
        int b = 7;
        int c = 12;

when(myCalculatorDao.save(12)).thenReturn(100);
        assertThat(myCalculator.add(a, b)).isEqualTo(c);
    }


    @Test
    public void sucessCase() {

        MyCalculator myCalculator = new MyCalculator(new MyCalculatorDaoFakeSucess());


        int a = 5;
        int b = 7;
        int c = 12;


        assertThat(myCalculator.add(a, b)).isEqualTo(c);
    }

    @Test
    public void testAdd2() {
        assertThat(myCalculator.add(5, 7)).isEqualTo(10);

    }

    @Test
    public void subTest() {
        assertThat(myCalculator.sub(8, 4)).isEqualTo(4);
    }

    @Test
    public void multiTest() {
        assertThat(myCalculator.multi(7, 4)).isEqualTo(28);
    }

    @Test
    public void divideTest() {
        assertThat(myCalculator.divide(12, 6)).isEqualTo(2);
    }

    @Test
    public void divideByZeroTest() {

        assertThatThrownBy(() -> myCalculator.divide(28, 0))

                .isInstanceOf(ArithmeticException.class)
                .hasMessage("/ by zero");
    }
}