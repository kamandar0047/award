package az.neft.mste.neft.entity;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
public class Stu {
    @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;



}