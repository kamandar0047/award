package az.neft.mste.neft.controller;

import az.neft.mste.neft.dto.CreateStuDto;
import az.neft.mste.neft.dto.StuDto;
import az.neft.mste.neft.services.StuService;
import az.neft.mste.neft.services.StuServiceImpl;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/stu")
public class StuController {

  private final StuService stuService;

    @GetMapping("/{id}")
    public StuDto get(Long id){
        log.trace("Get stu id",id);
        return stuService.getStu(id);
    }
  @PostMapping("/{id}")
    public ResponseEntity<StuDto> create(@RequestBody CreateStuDto dto){
        log.trace("create ",dto);
      StuDto stu = stuService.createStu(dto);
      return ResponseEntity.status(HttpStatus.CREATED).body(stuService.createStu(dto));

    }
    @PutMapping("/{id}")
    public StuDto update(@PathVariable Long id,@RequestBody StuDto dto){
      log.trace("update",id,dto);
        return stuService.updateStu(id,dto);
    }
@DeleteMapping("/{id}")
    public ResponseEntity delete (Long id){

       log.trace("delete ",id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
