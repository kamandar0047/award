package az.neft.mste.neft.repository;

import az.neft.mste.neft.entity.Stu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StuRepository extends JpaRepository<Stu,Long> {

}
