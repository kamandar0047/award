package az.neft.mste.neft.config;

import az.neft.mste.neft.services.SingletonService;
import az.neft.mste.neft.services.StuService;
import az.neft.mste.neft.services.StuServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.MatchingStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.ui.Model;

import java.util.UUID;

@Slf4j
@Configuration
public class Config {
   @Bean
   @Primary
   @Scope("prototype")
public SingletonService stuService(){
        log.trace("Created new stu servis");
SingletonService singletonService=new SingletonService();
singletonService.setId(UUID.randomUUID());
return singletonService;
}   @Bean
    @Scope("prototype")
    public SingletonService stuService2(){
        log.trace("Created new stu servis");
        SingletonService singletonService=new SingletonService();
        singletonService.setId(UUID.randomUUID());
        return singletonService;
    }



    public ModelMapper modelMapper(){
        ModelMapper modelMapper=new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
   return modelMapper;
    }


}
