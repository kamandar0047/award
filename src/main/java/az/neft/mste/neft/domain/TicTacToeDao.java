package az.neft.mste.neft.domain;

import lombok.Data;

import java.util.Date;

public interface TicTacToeDao {

    void saveMove (TicTacToeBean bean);

    void saveBoardState(Character[][] boardState);

    TicTacToeBean[] getGameHistory();
    TicTacToeBean getLastMove();

    void printResult(Date date);
}
