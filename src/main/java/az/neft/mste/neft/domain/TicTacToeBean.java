package az.neft.mste.neft.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TicTacToeBean {
    private Integer move;
    private Character player;
    private Integer x;
    private Integer y;



}
