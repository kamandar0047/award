package az.neft.mste.neft;

import az.neft.mste.neft.patterens.Singleton;
import az.neft.mste.neft.services.ApplicationDetails;
import az.neft.mste.neft.services.SingletonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;


@Slf4j
@RequiredArgsConstructor
@SpringBootApplication

public class NeftApplication implements CommandLineRunner {


private ApplicationDetails applicationDetails;
  //  private SingletonService singleton;

  //  private SingletonService singleton2;

   // public NeftApplication(@Qualifier("primary") SingletonService singleton, @Qualifier("secondary") SingletonService singleton2) {
    //    this.singleton = singleton;
    ///    this.singleton2 = singleton2;
   // }

    public static void main(String[] args) {
        SpringApplication.run(NeftApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
      //  log.trace("Application started", singleton);
      //  log.trace("Application started", singleton2);
        log.trace("The version is {} {} ",applicationDetails.getVersion(),

                applicationDetails.getDeveloper(),applicationDetails.getScores());


    }
}
