package az.neft.mste.neft.dto;

import javax.validation.constraints.NotBlank;

public class CreateStuDto {
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;

}