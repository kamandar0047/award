package az.neft.mste.neft.patterens;

public class Singleton {
private static Singleton instance;
    private Singleton(){}

   public static  Singleton getInstance(){

        if (instance ==null){
            synchronized (Singleton.class){
                if (instance==null){
                    instance=new Singleton();
                }
            }
            instance =new Singleton();
        }
        return instance;

   }
}
