package az.neft.mste.neft.patterens;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Car {

    private String make;
    private String model;
    private String year;



}
