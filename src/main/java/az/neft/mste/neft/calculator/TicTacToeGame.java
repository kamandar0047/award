package az.neft.mste.neft.calculator;

import az.neft.mste.neft.domain.TicTacToeBean;
import az.neft.mste.neft.domain.TicTacToeDao;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.pl.REGON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;

@Service

public class TicTacToeGame {
    char[][] board = new char[][]{{'\0', '\0', '\0'}
            , {'\0', '\0', '\0'}
            , {'\0', '\0', '\0'}};

    private Character lastPlayer='\0';


    public TicTacToeGame() {
        this(null);
    }

    public TicTacToeGame(TicTacToeDao ticTacToeDao) {
        this.ticTacToeDao = ticTacToeDao;
    }

    private  TicTacToeDao ticTacToeDao;


    public String play(int xCord, int yCord, Object lastPlayer) {

        checkCord(xCord,'X');
        checkCord(yCord,'Y');
        setTheBoard(xCord, yCord);

        printBoard();

       nextPlayer();
       lastPlayer();

       return  "No Winner";
    }

    public void nextPlayer() {
    }

    private  void lastPlayer() {
    }

    private void setTheBoard(int xCord,int yCord) {
        checkBoardEmpty(xCord, yCord);
        board[xCord-1][yCord-1] = 'X';
    }

    void checkCord(int cord,char c) {
        if (cord > 2 ||cord < 0)
            throw new RuntimeException(c+ "Cord is Runtime EXception");

    }



    void checkBoardEmpty(int xCord, int yCord) {
        if (board[xCord-1][yCord-1] != '\0')
            throw new RuntimeException("Board is occupied");
    }

    public void printBoard() {
        System.out.println("=== The Board Status Is ===");
        System.out.println(Arrays.deepToString(board));


    }
    public void saveMove(int x,int y){
        Date date=new Date();
ticTacToeDao.printResult(date);
ticTacToeDao.printResult(date);

//        ticTacToeDao.saveMove(null);

      //  TicTacToeBean lastMove=ticTacToeDao.getLastMove();
       // ticTacToeDao.saveMove(new TicTacToeBean(lastMove.getMove()+1,lastPlayer,x,y));
    }


    public void play(int i, int i1) {
    }
}
