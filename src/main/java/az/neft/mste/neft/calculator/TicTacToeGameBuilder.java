package az.neft.mste.neft.calculator;

import az.neft.mste.neft.domain.TicTacToeDao;

public class TicTacToeGameBuilder {
    private TicTacToeDao ticTacToeDao;

    public TicTacToeGameBuilder setTicTacToeDao(TicTacToeDao ticTacToeDao) {
        this.ticTacToeDao = ticTacToeDao;
        return this;
    }

    public TicTacToeGame createTicTacToeGame() {
        return new TicTacToeGame(ticTacToeDao);
    }
}