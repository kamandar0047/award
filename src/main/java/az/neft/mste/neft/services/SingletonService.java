package az.neft.mste.neft.services;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Data
//@Scope("prototype")

public class SingletonService implements ISingletonService {
    private UUID id;

}
