package az.neft.mste.neft.services;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;

@Data
@Configuration
@ConfigurationProperties(prefix = "application")
public class ApplicationDetails {
    private String version;

    private List developer;
    private Map<String ,Integer> scores;


}
