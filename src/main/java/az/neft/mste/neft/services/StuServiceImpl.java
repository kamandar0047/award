package az.neft.mste.neft.services;

import az.neft.mste.neft.dto.CreateStuDto;
import az.neft.mste.neft.dto.StuDto;
import az.neft.mste.neft.entity.Stu;
import az.neft.mste.neft.repository.StuRepository;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import java.util.Optional;

@Slf4j
@Service
public class StuServiceImpl implements StuService {

    private StuRepository stuRepository;
private ModelMapper modelMapper;

    public StuDto getStu(Long id){
        log.trace("get student",id);
        Optional<Stu> optionalStu = stuRepository.findById(id);
        Stu stu = optionalStu.get();
        return modelMapper.map(stu,StuDto.class);
}

    public StuDto createStu(CreateStuDto dto){
        log.trace("create student",dto);
        Stu stu = stuRepository.save(modelMapper.map(dto,Stu.class));

        return modelMapper.map(stu,StuDto.class);
    }
    public StuDto updateStu(Long id, StuDto dto){
        log.trace("update student",id,dto);
        Stu stu = modelMapper.map(dto, Stu.class);
        stu.setId(id);

        stuRepository.save(stu);
        return modelMapper.map(stu, StuDto.class);
    }
    public void deleteStu(Long id){
        log.trace("delete student",id);
    }
}
