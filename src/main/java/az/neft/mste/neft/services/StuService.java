package az.neft.mste.neft.services;

import az.neft.mste.neft.dto.CreateStuDto;
import az.neft.mste.neft.dto.StuDto;

public interface StuService {
    StuDto getStu(Long id);

    StuDto createStu(CreateStuDto dto);

    StuDto updateStu(Long id, StuDto dto);

    void deleteStu(Long id);

}
